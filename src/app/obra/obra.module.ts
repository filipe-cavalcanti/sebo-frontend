import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ObraRoutingModule } from './obra-routing.module';
import { ObraEditorComponent } from './obra-editor/obra-editor.component';
import {ReactiveFormsModule} from '@angular/forms';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatPaginatorModule, MatSelectModule,
  MatTableModule, MatTooltipModule
} from '@angular/material';
import { ObraCadastroComponent } from './obra-cadastro/obra-cadastro.component';
import { ObraListComponent } from './obra-list/obra-list.component';
import { ObraEdicaoComponent } from './obra-edicao/obra-edicao.component';


@NgModule({
  declarations: [ObraEditorComponent, ObraCadastroComponent, ObraListComponent, ObraEdicaoComponent],
  imports: [
    CommonModule,
    ObraRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatCardModule,
    MatButtonModule,
    MatTableModule,
    MatIconModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatSelectModule
  ]
})
export class ObraModule { }
