import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ObraCadastroComponent} from './obra-cadastro/obra-cadastro.component';
import {ObraListComponent} from './obra-list/obra-list.component';
import {ObraEdicaoComponent} from './obra-edicao/obra-edicao.component';


const OBRA_ROUTE: Routes = [
  {path: 'cadastrar-obra', component: ObraCadastroComponent},
  {path: 'listar-obras', component: ObraListComponent},
  {path: 'editar-obra/:idObra', component: ObraEdicaoComponent},
];

@NgModule({
  imports: [RouterModule.forChild(OBRA_ROUTE)],
  exports: [RouterModule]
})
export class ObraRoutingModule { }
