import { Component, OnInit } from '@angular/core';
import {ObraListagemModel} from '../models/obra-listagem.model';
import {ObraService} from '../obra.service';
import {PageEvent} from '@angular/material';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Page} from '../../infraestrutura/page';
import {FiltroModel} from '../models/filtro.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-obra-list',
  templateUrl: './obra-list.component.html',
  styleUrls: ['./obra-list.component.css']
})
export class ObraListComponent implements OnInit {

  public obras: Array<ObraListagemModel>;
  public quantidadeObras: number;
  public page: PageEvent = new PageEvent();

  public filtroForm: FormGroup;

  public displayedColumns: string[] = ['nome', 'descricao', 'dataPublicacao', 'dataExposicao', 'acoes'];

  constructor(private obraService: ObraService,
              private fb: FormBuilder,
              private route: Router) { }

  ngOnInit() {
    this.page.pageSize = 15;
    this.page.pageIndex = 0;
    this.filtroForm = this.fb.group({
      nomeAutor: [''],
      nomeObra: [''],
      descricaoObra: ['']
    });
    this.consultarQuantidade(this.filtroForm.value);
    this.consultar(this.page);
  }

  public onPageChange(page: PageEvent): void {
    this.page = page;
    this.consultar(page);
  }

  public consultar(pageEvent: PageEvent): void {
    this.obraService.consultar(this.filtroForm.value as FiltroModel, pageEvent.pageIndex, pageEvent.pageSize)
      .subscribe((page: Page<ObraListagemModel>) => {
      this.obras = page.content;
    });
  }

  public consultarQuantidade(filtro: FiltroModel) {
    this.obraService.consultarQuantidade(filtro).subscribe(quantidade => this.quantidadeObras = quantidade);
  }

  public pesquisar(): void {
    this.consultarQuantidade(this.filtroForm.value);
    this.consultar(this.page);
  }

  public editar(id: number): void {
    this.route.navigate([`editar-obra/${id}`]);
  }

  public remover(id: number) {
    this.obraService.remover(id).subscribe(result => {
      alert(result.mensagem);
      this.consultar(this.page);
    });
  }
}
