import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObraEditorComponent } from './obra-editor.component';

describe('ObraEditorComponent', () => {
  let component: ObraEditorComponent;
  let fixture: ComponentFixture<ObraEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObraEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObraEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
