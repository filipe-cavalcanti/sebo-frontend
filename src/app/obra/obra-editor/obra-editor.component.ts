import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {dataObraValidator} from '../infraestrutura/validators';
import {ObraModel} from '../models/obra.model';
import {AutorSelecaoModel} from '../../autor/models/autor-selecao.model';
import {AutorService} from '../../autor/autor.service';

@Component({
  selector: 'app-obra-editor',
  templateUrl: './obra-editor.component.html',
  styleUrls: ['./obra-editor.component.css']
})
export class ObraEditorComponent implements OnInit {
  public fileData;
  public obraForm: FormGroup;
  public imagemUrl: any;
  public autoresSelecao: Array<AutorSelecaoModel>;

  @Input() textButton;

  @Input() obraModelEdicao: ObraModel;

  @Output() obraModelEmitter: EventEmitter<ObraModel>;

  constructor(private fb: FormBuilder, private autorService: AutorService) {
    this.obraModelEmitter = new EventEmitter<ObraModel>();
  }

  ngOnInit() {
    this.obraForm = this.fb.group({
      nome: ['', Validators.required],
      descricao: ['', Validators.required],
      imagem: ['', Validators.required],
      dataPublicacao: [''],
      dataExposicao: [''],
      autores: [[]]
    }, {validators: dataObraValidator});
    if (this.obraModelEdicao) {
      this.obraForm.setValue(this.obraModelEdicao);
      this.showImagem(this.obraModelEdicao.imagem);
    }
    this.consultarAutoresSelecao();
  }

  public uploadArquivo($event: any): void {
    this.fileData = $event.target.files[0] as File;
    this.obraForm.get('imagem').setValue(this.fileData);
    this.showPreview($event);
  }

  public showPreview(event): void {
    const file = (event.target as HTMLInputElement).files[0];
    this.showImagem(file);
  }

  public onSubmit(): void {
    this.obraModelEmitter.emit(this.obraForm.value);
  }

  public showImagem(imagem): void {
    const reader = new FileReader();
    reader.onload = () => {
      this.imagemUrl = reader.result as string;
    };
    reader.readAsDataURL(imagem);
  }

  public consultarAutoresSelecao(): void {
    this.autorService.consultarParaSelecao().subscribe((autores: Array<AutorSelecaoModel>) => this.autoresSelecao = autores);
  }
}
