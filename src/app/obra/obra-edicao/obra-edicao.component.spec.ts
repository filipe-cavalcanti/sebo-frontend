import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObraEdicaoComponent } from './obra-edicao.component';

describe('ObraEdicaoComponent', () => {
  let component: ObraEdicaoComponent;
  let fixture: ComponentFixture<ObraEdicaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObraEdicaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObraEdicaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
