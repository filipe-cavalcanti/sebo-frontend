import {Component, OnInit} from '@angular/core';
import {ObraModel} from '../models/obra.model';
import {ObraService} from '../obra.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-obra-edicao',
  templateUrl: './obra-edicao.component.html',
  styleUrls: ['./obra-edicao.component.css']
})
export class ObraEdicaoComponent implements OnInit {

  public id: number;
  public obraModel: ObraModel;
  public readyObraModel: boolean;

  constructor(private obraService: ObraService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(result => {
      this.id = parseInt(result.get('idObra'), 10);
      this.obraService.consultarPorId(this.id).subscribe((obraModel: ObraModel) => this.obraModel = obraModel);
    });
    this.consultarImagen(this.id);
  }

  public editar(obraModel: ObraModel): void {
    this.obraService.editar(obraModel, this.id).subscribe(result => alert(result.mensagem), error => alert(error.error.message));
  }

  public consultarImagen(id: number): void {
    this.obraService.consultarImagem(id).subscribe(imagem => {
      const b: any = imagem;
      b.lastModifiedDate = new Date();
      b.name = 'image.png';
      this.obraModel.imagem = imagem as File;
      this.readyObraModel = true;
    });
  }
}
