import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

export const dataObraValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const dataPublicacao = control.get('dataPublicacao').value;
  const dataExposicao = control.get('dataExposicao').value;
  return !dataPublicacao && !dataExposicao ? { cpfInvalid: true } : null;
};
