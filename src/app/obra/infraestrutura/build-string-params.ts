import {HttpParams} from '@angular/common/http';

export function buildParams(...objects: any): HttpParams {
  let params = new HttpParams();
  objects.forEach(object => {
    Object.keys(object).forEach(key => {
      params = params.set(key, object[key]);
    });
  });
  return params;
}
