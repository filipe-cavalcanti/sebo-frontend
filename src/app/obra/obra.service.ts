import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ObraModel} from './models/obra.model';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {FiltroModel} from './models/filtro.model';
import {ObraListagemModel} from './models/obra-listagem.model';
import {Page} from '../infraestrutura/page';
import {buildParams} from './infraestrutura/build-string-params';
import {ObraSelecaoModel} from './models/obra-selecao.model';

@Injectable({
  providedIn: 'root'
})
export class ObraService {
  private static END_POINT = 'obras';

  constructor(private http: HttpClient) { }

  static mapper(obraModel: ObraModel): FormData {
    const formData = new FormData();
    formData.set('imagem', obraModel.imagem);
    formData.set('nome', obraModel.nome);
    formData.set('descricao', obraModel.descricao);
    formData.set('dataPublicacao', obraModel.dataPublicacao !== null ? JSON.stringify(obraModel.dataPublicacao) : '');
    formData.set('dataExposicao', obraModel.dataExposicao !== null ? JSON.stringify(obraModel.dataExposicao) : '');
    formData.set('autores', JSON.stringify(obraModel.autores));
    return formData;
  }

  public cadastrar(obraModel: ObraModel): Observable<any> {
    return this.http.post(`${environment.BASE_URL}/${ObraService.END_POINT}`, ObraService.mapper(obraModel));
  }

  public consultar(filtro: FiltroModel, pageIndex, pageSize): Observable<Page<ObraListagemModel>> {
    return this.http.get<Page<ObraListagemModel>>(`${environment.BASE_URL}/${ObraService.END_POINT}`, {
      params: buildParams(filtro).set('pageIndex', pageIndex).set('pageSize', pageSize)
    });
  }

  public consultarParaSelecao(): Observable<Array<ObraSelecaoModel>> {
    return this.http.get<Array<ObraSelecaoModel>>(`${environment.BASE_URL}/${ObraService.END_POINT}/selecao`);
  }

  public consultarQuantidade(filtro: FiltroModel): Observable<number> {
    return this.http.get<number>(`${environment.BASE_URL}/${ObraService.END_POINT}/quantidade`, {
      params: buildParams(filtro)
    });
  }

  public editar(obraModel: ObraModel, id: number): Observable<any> {
    return this.http.put(`${environment.BASE_URL}/${ObraService.END_POINT}/${id}`, ObraService.mapper(obraModel));
  }

  public consultarPorId(id: number): Observable<ObraModel> {
    return this.http.get<ObraModel>(`${environment.BASE_URL}/${ObraService.END_POINT}/${id}`);
  }

  public consultarImagem(id: number): Observable<any> {
    return this.http.get(`${environment.BASE_URL}/${ObraService.END_POINT}/${id}/imagem`, {
      responseType: 'blob'
    });
  }

  public remover(id: number): Observable<any> {
    return this.http.delete(`${environment.BASE_URL}/${ObraService.END_POINT}/${id}`);
  }

}
