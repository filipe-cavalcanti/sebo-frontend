export interface ObraListagemModel {
  id: number;
  nome: string;
  descricao: string;
  dataPublicacao: Date;
  dataExposicao: Date;
}
