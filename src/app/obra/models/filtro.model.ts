export interface FiltroModel {
  nomeAutor: string;
  nomeObra: string;
  descricaoObra: string;
}
