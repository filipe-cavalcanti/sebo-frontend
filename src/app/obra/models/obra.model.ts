export interface ObraModel {
  nome: string;
  descricao: string;
  dataPublicacao: Date;
  dataExposicao: Date;
  imagem: File;
  autores: Array<number>;
}
