import { Component, OnInit } from '@angular/core';
import {ObraService} from '../obra.service';
import {ObraModel} from '../models/obra.model';

@Component({
  selector: 'app-obra-cadastro',
  templateUrl: './obra-cadastro.component.html',
  styleUrls: ['./obra-cadastro.component.css']
})
export class ObraCadastroComponent implements OnInit {

  constructor(private obraService: ObraService) { }

  ngOnInit() {
  }

  public cadastrar(obraModel: ObraModel): void {
    this.obraService.cadastrar(obraModel).subscribe(result => {
      alert(result.mensagem);
    }, error => alert(error.error.message));
  }
}
