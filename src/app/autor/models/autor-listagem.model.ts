export interface AutorListagemModel {
  id: number;
  nome: string;
  dataNascimento: string;
  paisOrigem: string;
}
