export interface PaisSelecaoModel {
  id: number;
  codigo: string;
  nome: string;
}
