import {SexoEnum} from '../infraestrutura/sexo.enum';

export interface AutorModel {
  id?: number;
  nome: string;
  sexo: SexoEnum;
  email: string;
  dataNascimento: Date;
  cpf: string;
  paisOrigem: number;
  obras: Array<number>;
}
