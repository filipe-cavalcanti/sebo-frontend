import { Component, OnInit } from '@angular/core';
import {AutorModel} from '../models/autor.model';
import {AutorService} from '../autor.service';

@Component({
  selector: 'app-autor-cadastro',
  templateUrl: './autor-cadastro.component.html',
  styleUrls: ['./autor-cadastro.component.css']
})
export class AutorCadastroComponent implements OnInit {

  constructor(private autorService: AutorService) { }

  ngOnInit() {
  }

  public cadastrar(autorModel: AutorModel): void {
    this.autorService.cadastrar(autorModel).subscribe(result => {
      alert(result.mensagem);
    }, error => alert(error.error.message));
  }

}
