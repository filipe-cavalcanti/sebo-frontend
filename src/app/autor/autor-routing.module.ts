import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AutorCadastroComponent} from './autor-cadastro/autor-cadastro.component';
import {AutorEdicaoComponent} from './autor-edicao/autor-edicao.component';
import {AutorListComponent} from './autor-list/autor-list.component';


const autorRoute: Routes = [
  {path: 'cadastrar-autor', component: AutorCadastroComponent},
  {path: 'editar-autor/:idAutor', component: AutorEdicaoComponent},
  {path: 'listar-autores', component: AutorListComponent},
];

@NgModule({
  imports: [RouterModule.forChild(autorRoute)],
  exports: [RouterModule]
})
export class AutorRoutingModule { }
