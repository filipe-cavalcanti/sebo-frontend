import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AutorModel} from './models/autor.model';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {AutorListagemModel} from './models/autor-listagem.model';
import {AutorSelecaoModel} from './models/autor-selecao.model';

@Injectable({
  providedIn: 'root'
})
export class AutorService {

  private static END_POINT = 'autores';

  constructor(private http: HttpClient) { }

  public cadastrar(autorModel: AutorModel): Observable<any> {
    return this.http.post(`${environment.BASE_URL}/${AutorService.END_POINT}`, autorModel);
  }

  public consultarPorId(id: number): Observable<AutorModel> {
    return this.http.get<AutorModel>(`${environment.BASE_URL}/${AutorService.END_POINT}/${id}`);
  }

  public editar(autorModel: AutorModel, id: number): Observable<any> {
    return this.http.put(`${environment.BASE_URL}/${AutorService.END_POINT}/${id}`, autorModel);
  }

  public consultar(): Observable<Array<AutorListagemModel>> {
    return this.http.get<Array<AutorListagemModel>>(`${environment.BASE_URL}/${AutorService.END_POINT}`);
  }

  public remover(id: number): Observable<any> {
    return this.http.delete(`${environment.BASE_URL}/${AutorService.END_POINT}/${id}`);
  }

  public consultarParaSelecao(): Observable<Array<AutorSelecaoModel>> {
    return this.http.get<Array<AutorSelecaoModel>>(`${environment.BASE_URL}/${AutorService.END_POINT}/selecao`);
  }
}
