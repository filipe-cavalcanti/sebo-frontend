import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorEditorComponent } from './autor-editor.component';

describe('AutorEditorComponent', () => {
  let component: AutorEditorComponent;
  let fixture: ComponentFixture<AutorEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
