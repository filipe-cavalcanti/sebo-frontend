import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AutorModel} from '../models/autor.model';
import {SexoEnum} from '../infraestrutura/sexo.enum';
import {CPF_MASK} from '../infraestrutura/masks';
import {ObraSelecaoModel} from '../../obra/models/obra-selecao.model';
import {ObraService} from '../../obra/obra.service';
import {PaisService} from '../pais.service';
import {PaisSelecaoModel} from '../models/pais-selecao.model';
import {MatSelectChange} from '@angular/material';

@Component({
  selector: 'app-autor-editor',
  templateUrl: './autor-editor.component.html',
  styleUrls: ['./autor-editor.component.css']
})
export class AutorEditorComponent implements OnInit {
  public static BRASIL = 'Brazil';
  public CPF_MASK = CPF_MASK;
  public autorForm: FormGroup;
  public sexoEnum = SexoEnum;
  public paisesSelecao: Array<PaisSelecaoModel>;
  public brazil: PaisSelecaoModel;
  private flagIsBrazil;

  public obras: Array<ObraSelecaoModel>;

  @Input() autor: AutorModel;
  @Input() textButtom: string;

  @Output() autorEventEmitter: EventEmitter<AutorModel>;

  constructor(private fb: FormBuilder,
              private obraService: ObraService,
              private paisService: PaisService) {
    this.autorEventEmitter = new EventEmitter<AutorModel>();
  }

  ngOnInit() {
    this.consultarObrasParaSelecao();
    this.consultarPaisesParaSelecao();
    this.autorForm = this.fb.group({
      nome: ['', Validators.required],
      sexo: [],
      email: ['', Validators.email],
      dataNascimento: ['', Validators.required],
      paisOrigem: ['', Validators.required],
      cpf: [null],
      obras: [[]],
    }, );
    if (this.autor) {
      this.autorForm.setValue(this.autor);
    }

  }

  public onSubmit(): void {
    this.autorEventEmitter.emit(this.autorForm.value);
  }

  public consultarObrasParaSelecao(): void {
    this.obraService.consultarParaSelecao().subscribe((obras: Array<ObraSelecaoModel>) => this.obras = obras);
  }

  public consultarPaisesParaSelecao(): void {
    this.paisService.consultarParaSelecao().subscribe((paises: Array<PaisSelecaoModel>) => {
      this.paisesSelecao = paises;
      this.brazil = this.paisesSelecao.find(pais => pais.nome === AutorEditorComponent.BRASIL);
      this.flagIsBrazil = this.brazil !== null && this.autor.paisOrigem === this.brazil.id;
    });
  }

  public isBrasil(event: MatSelectChange): void {
    this.flagIsBrazil = event.value === this.brazil.id;
  }


  get isBrazil(): boolean {
    return this.flagIsBrazil;
  }
}
