import { Component, OnInit } from '@angular/core';
import {AutorListagemModel} from '../models/autor-listagem.model';
import {AutorService} from '../autor.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-autor-list',
  templateUrl: './autor-list.component.html',
  styleUrls: ['./autor-list.component.css']
})
export class AutorListComponent implements OnInit {

  public autores: Array<AutorListagemModel>;

  public displayedColumns: string[] = ['nome', 'dataNascimento', 'paisOrigem', 'acoes'];

  constructor(private autorService: AutorService,
              private route: Router) { }

  ngOnInit() {
    this.consultarAutores();
  }

  public consultarAutores(): void {
    this.autorService.consultar().subscribe(autores => this.autores = autores);
  }

  public editar(id: number): void {
    this.route.navigate([`editar-autor/${id}`]);
  }

  public remover(id: number): void {
    this.autorService.remover(id).subscribe(result => {
      alert(result.mensagem);
      this.consultarAutores();
    }, error => alert(error.error.message));
  }

}
