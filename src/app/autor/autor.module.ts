import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AutorRoutingModule} from './autor-routing.module';
import {AutorEditorComponent} from './autor-editor/autor-editor.component';
import {TextMaskModule} from 'angular2-text-mask';
import { AutorCadastroComponent } from './autor-cadastro/autor-cadastro.component';
import { AutorEdicaoComponent } from './autor-edicao/autor-edicao.component';
import { AutorListComponent } from './autor-list/autor-list.component';
import {
  MatButtonModule,
  MatCardModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatNativeDateModule,
  MatSelectModule,
  MatTableModule
} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AutorEditorComponent,
    AutorCadastroComponent,
    AutorEdicaoComponent,
    AutorListComponent
  ],
  imports: [
    CommonModule,
    AutorRoutingModule,
    TextMaskModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
  ]
})
export class AutorModule { }
