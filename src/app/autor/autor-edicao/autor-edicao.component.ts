import {Component, OnInit} from '@angular/core';
import {AutorService} from '../autor.service';
import {ActivatedRoute} from '@angular/router';
import {AutorModel} from '../models/autor.model';

@Component({
  selector: 'app-autor-edicao',
  templateUrl: './autor-edicao.component.html',
  styleUrls: ['./autor-edicao.component.css']
})
export class AutorEdicaoComponent implements OnInit {

  id: number;
  autorEdicao: AutorModel;

  constructor(private autorService: AutorService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(result => {
      this.id = parseInt(result.get('idAutor'), 10);
      this.autorService.consultarPorId(this.id).subscribe((autorModel: AutorModel) => this.autorEdicao = autorModel);
    });
  }

  public editar(autorModel: AutorModel): void {
    this.autorService.editar(autorModel, this.id).subscribe(result => {
      alert(result.mensagem);
    }, error => alert(error.error.message));
  }

}
