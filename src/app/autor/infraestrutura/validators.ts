import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';
import {CPF_REGEX} from './regex-util';

export const brasilDocumentoValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const name = control.get('paisOrigem').value as string;
  const cpf = control.get('cpf').value as string;
  return name.toLowerCase() === 'brasil' && !CPF_REGEX.test(cpf) ? { cpfInvalid: true } : null;
};
