import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PaisSelecaoModel} from './models/pais-selecao.model';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  private END_POINT = 'paises';

  constructor(private http: HttpClient) { }

  public consultarParaSelecao(): Observable<Array<PaisSelecaoModel>> {
    return this.http.get<Array<PaisSelecaoModel>>(`${environment.BASE_URL}/${this.END_POINT}`);
  }

}
