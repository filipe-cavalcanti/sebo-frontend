export interface Page<T> {
  content: Array<T>;
  pageable: string;
  totalElements: number;
  totalPages: number;
  last: boolean;
  first: boolean;
  sort: any;
  numberOfElements: number;
  size: number;
  number: number;
  empty: number;
}
